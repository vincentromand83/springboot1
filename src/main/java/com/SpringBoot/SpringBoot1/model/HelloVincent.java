package com.SpringBoot.SpringBoot1.model;

public class HelloVincent {

    private String value = "Hello Vincent!";

    public String getValue() { return value; }

    public void setValue(String value) { this.value = value; }

    @Override
    public String toString() {
        return value;
    }
}
