package com.SpringBoot.SpringBoot1;

import com.SpringBoot.SpringBoot1.model.HelloVincent;
import com.SpringBoot.SpringBoot1.service.BusinessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBoot1Application implements CommandLineRunner {

	@Autowired
	private BusinessService bs;

	public static void main(String[] args) {
		SpringApplication.run(SpringBoot1Application.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		HelloVincent hv = bs.getHellovincent();
		System.out.println(hv);
	}
}
